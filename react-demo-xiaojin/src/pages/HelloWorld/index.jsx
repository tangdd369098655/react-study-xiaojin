import * as React from 'react';

const HelloWorld = () => {
  function MyButton() {
    return <button>I'm a button</button>;
  }
  return (
    <div>
      <h1>Welcome to my app</h1>
      <MyButton />
    </div>
  );
};

export default HelloWorld;

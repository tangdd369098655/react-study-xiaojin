import { lazy } from 'ice';
import BasicLayout from '@/layouts/BasicLayout';

const Dashboard = lazy(() => import('@/pages/Dashboard'));
const HelloWorld = lazy(() => import('@/pages/HelloWorld'));
const routerConfig = [
  {
    path: '/',
    component: BasicLayout,
    children: [
      {
        path: '/HelloWorld',
        component: HelloWorld,
      },
      {
        path: '/',
        component: Dashboard,
      },
    ],
  },
];
export default routerConfig;

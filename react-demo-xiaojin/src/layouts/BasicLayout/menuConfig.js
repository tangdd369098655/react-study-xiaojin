const headerMenuConfig = [];
const asideMenuConfig = [
  {
    name: 'Dashboard',
    path: '/',
    icon: 'smile',
  },
  {
    name: 'HelloWorld',
    path: '/HelloWorld',
    icon: 'smile',
  },
];
export { headerMenuConfig, asideMenuConfig };
